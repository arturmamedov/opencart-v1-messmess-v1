<?php 
    foreach($messages as $msg):
    if($msg['user_id'] == $other_user_id):
	if($msg['type'] == 'file-img'):
?>
		<!-- img messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
			<?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
				<img src="<?php echo HTTP_CATALOG; ?>image/message/admin.png">
                <p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch">
                    <li class="">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
			<?php else: ?>
				<a class="msgDelete msg-btn-delete">x</a>
				<img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
				<p class="msgSwitch">
				<a href="<?php echo $msg['message'];?>" target="_blank">
					<div class="preview">
						<img class="lfloat" src="<?php echo $msg['message'];?>">
					</div>
					<?php echo $msg['title'];?>
				</a>
				</p>
				<ul class="msgSwitch display-none">
                    <li>Elimina? 
                        <span class="noDelete btn btn-primary btn-mini">No</span> 
                        <span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
                    </li>
                    <li class="display-none">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
			<?php endif; ?>
			<span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php elseif($msg['type'] == 'file'): ?>
		<!-- file messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
		<?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
			<img src="<?php echo HTTP_CATALOG; ?>image/message/admin.png">
                <p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch">
                    <li class="">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
		<?php else: ?>
			<a class="msgDelete msg-btn-delete">x</a>
            <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
            <p class="msgSwitch">
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview docfile">
					<img class="lfloat" src="<?php echo HTTP_CATALOG; ?>image/docfile.png">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
			<ul class="msgSwitch display-none">
				<li>Elimina? 
					<span class="noDelete btn btn-primary btn-mini">No</span> 
					<span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
				</li>
				<li class="display-none">
					<a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
				</li>
			</ul>
		<?php endif; ?>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>	
        </li>
<?php else: ?>
		<!-- text messages -->
        <li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
            <?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/admin.png">
                <p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch">
                    <li class="">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
            <?php else: ?>
                <a class="msgDelete msg-btn-delete">x</a>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/admin.png">
                <p class="msgSwitch"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch display-none">
                    <li>Elimina? 
                        <span class="noDelete btn btn-primary btn-mini">No</span> 
                        <span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
                    </li>
                    <li class="display-none">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
            <?php endif; ?>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php 
    endif;
    else:
	// other user messages
	if($msg['type'] == 'file-img'):
?>
		<!-- image messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
		<?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
			<img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
			<p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
			<ul class="msgSwitch">
				<li class="">
					<a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
				</li>
			</ul>
		<?php else: ?>
			<a class="msgDelete msg-btn-delete">x</a>
            <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
            <p class="msgSwitch">
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview">
					<img class="lfloat" src="<?php echo $msg['message'];?>">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
			<ul class="msgSwitch display-none">
				<li>Elimina? 
					<span class="noDelete btn btn-primary btn-mini">No</span> 
					<span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
				</li>
				<li class="display-none">
					<a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
				</li>
			</ul>
			<?php endif; ?>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php elseif($msg['type'] == 'file'): ?>
		<!-- file messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
		<?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
			<img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
			<p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
			<ul class="msgSwitch">
				<li class="">
					<a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
				</li>
			</ul>
		<?php else: ?>
			<a class="msgDelete msg-btn-delete">x</a>
            <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
            <p class="msgSwitch">
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview docfile">
					<img class="lfloat" src="<?php echo HTTP_CATALOG; ?>image/docfile.png">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
			<ul class="msgSwitch display-none">
				<li>Elimina? 
					<span class="noDelete btn btn-primary btn-mini">No</span> 
					<span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
				</li>
				<li class="display-none">
					<a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
				</li>
			</ul>
			<?php endif; ?>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php else: ?>
		<!-- text messages -->
        <li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
            <?php if($msg['one_active'] == 0 || $msg['two_active'] == 0 ): ?>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
                <p class="msgSwitch" style="color: #AAAAAA;"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch">
                    <li class="">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
            <?php else: ?>
                <a class="msgDelete msg-btn-delete">x</a>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
                <p class="msgSwitch"><?php echo $msg['message'];?></p>
                <ul class="msgSwitch display-none">
                    <li>Elimina? 
                        <span class="noDelete btn btn-primary btn-mini">No</span> 
                        <span id="<?php echo $msg['id']; ?>" class="yesDelete btn btn-danger btn-mini">Si</span>
                    </li>
                    <li class="display-none">
                        <a class="cancelDelete" id="<?php echo $msg['id'];?>" href="javascript:;">Annulla</a>
                    </li>
                </ul>
            <?php endif; ?>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php 
    endif;
	endif;
endforeach;
?>