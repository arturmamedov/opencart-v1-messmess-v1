<?php
if(count($conversations) > 0):
?>
<div class="text-center msg-link">
	<a class="<?php echo $order_last; ?>" href="<?php echo $this->url->link('message/message', 'path='.$message_path. '&order=last&' . 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Messaggi Recenti" class="btn btn-large rfloat">Più recente</a>
	
	<a class="<?php echo $order_first; ?>" href="<?php echo $this->url->link('message/message', 'path='.$message_path. '&order=first&' . 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Messaggi Recenti" class="btn btn-large rfloat">Meno recente</a>
	
	<a class="<?php echo $order_az; ?>" href="<?php echo $this->url->link('message/message', 'path='.$message_path. '&order=az&' . 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Dalla A alla Z" class="btn btn-large rfloat">A-Z</a>
	
	<a class="<?php echo $order_za; ?>" href="<?php echo $this->url->link('message/message', 'path='.$message_path. '&order=za&' . 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Dalla Z alla A" class="btn btn-large rfloat">Z-A</a>
</div>

<?php
foreach($conversations as $cnv):
?>
    <div id="<?php echo (int)$cnv['id']; ?>" class="msg-user" uid="<?php echo $cnv['customer_id'];?>" data-url="<?php echo $this->url->link('message/message/getmessages', 'token=' . $this->session->data['token'], 'SSL'); ?>">
		<img src="<?php echo HTTP_CATALOG; ?>image/message/user.png" class="lfloat">
		
		<div class="lfloat">
			<small><?php echo $cnv['ts_last'];?></small>
			<h3 class="no-margin no-padding"><?php echo $cnv['firstname'].' '.$cnv['lastname']; ?></h3>
			<h5 class="no-margin"><?php echo $cnv['email']; ?></h5>
		</div>
	</div>
<?php
endforeach;
else:
?>
<p>Nessun utente</p>
<?php
endif;
?>