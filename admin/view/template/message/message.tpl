<?php echo $header; ?>
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-ui-noscript.css"></noscript>

<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<div class="box">
  <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
       
        <div class="rfloat msg-link">
            <a class="<?php echo $path_inbox; ?>" href="<?php echo $this->url->link('message/message', 'token=' . $this->session->data['token'], 'SSL'); ?>" title="Messaggi Recenti" class="btn btn-large rfloat">In entrata</a>
            <a class="<?php echo $path_trash; ?>" href="<?php echo $this->url->link('message/message', 'path=trash&token=' . $this->session->data['token'], 'SSL'); ?>" title="Messaggi Eliminati" class="btn btn-large rfloat">Archiviati</a>
            <a class="<?php echo $path_all; ?>" href="<?php echo $this->url->link('message/message', 'path=all&token=' . $this->session->data['token'], 'SSL'); ?>" title="Tutti i Messaggi" class="btn btn-large rfloat">Tutti</a>
        </div>
    </div>
  <div class="content">
       <div class="box-content withChat">         
        <div class="newCnv msg-chat-header rfloat">
            <input type="hidden" id="hidden_url" value="<?php echo $this->url->link('message/message/deletemessage', 'token=' . $this->session->data['token'], 'SSL'); ?>">
            <input type="hidden" id="hidden_url_undelete" value="<?php echo $this->url->link('message/message/undeletemessage', 'token=' . $this->session->data['token'], 'SSL'); ?>">
			<input type="hidden" id="hidden_url_listen" value="<?php echo $this->url->link('message/message/listen', 'token=' . $this->session->data['token'], 'SSL'); ?>">
            <a href="javascript:;" id="cnvOpt" class="user-onoff display-none">Opzioni</a>
            <ul id="cnvOptMenu" class="list-unstyled cnv-opt-menu display-none">
                <!-- <li class="setUnRead lfloat">
                    <a href="javascript:;">Imposta come non letto</a>
                </li> -->
                <li class="delete lfloat">
                    <a href="javascript:;">Elimina</a>
                </li>
                <li class="display-none"><span class="noDelete btn btn-primary btn-mini">No</span> 
                    <span class="yesDelete btn btn-danger btn-mini" data-url="<?php echo $this->url->link('message/message/totrash', 'token=' . $this->session->data['token'], 'SSL'); ?>">Yes</span>
                </li>
                <div class="clearfix"></div>
            </ul>
        </div> 

        <div class="msg-container lfloat">
            <div class="msg-users admin">
                <?php 
                    if(isset($conversations)){
                        if(strlen($path_all) > 2){
                            include DIR_TEMPLATE.'message/libs/all_conversations.tpl';
                        } else {
                            include DIR_TEMPLATE.'message/libs/conversations.tpl';
                        }
                    }
                ?>
            </div>
        </div>
        
        <ul class="msg-chat rfloat">
            <div class="msgError alert alert-danger alert-block display-none posAbs" style="bottom:0px; margin-bottom: 0px;"></div>
            
            <?php if(isset($messages))
                include DIR_TEMPLATE.'message/libs/messages.tpl';
            ?>
        </ul>

        <div class="msg-write rfloat">
            <form action="<?php echo $action ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="user_id" value="<?php echo $other_user_id; ?>" required>
                <input type="hidden" name="cnv_id" value="<?php echo $cnv_id; ?>">
				<textarea required autofocus class="lfloat" name="msgtxt" disabled></textarea>
                <input type="submit" value="Invia" class="btn btn-success btn-lg rfloat" disabled>
            </form>
			<div class="clearfix"></div>
			<div class="dropzone display-none rfloat">
                <!-- The file upload form used as target for the file upload widget -->
                <form id="fileupload" action="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/server/php/" method="POST" enctype="multipart/form-data">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                    <noscript>Devi abilitare i Javascript nel Browser per poter caricare immagini (oppure riprova con un altro Borwser es: Chrome, FireFox, Opera)</noscript>
                    
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar">
                        <div class="text-right">
                            
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-info fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Invia file...</span>
                                <input type="file" name="files[]">
                            </span>
                            
                            <!-- The global file processing state -->
                            <span class="fileupload-process"></span>
                        </div>
                        
                        <!-- The global progress state -->
                        <div class="fileupload-progress fade">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                            </div>
                            <!-- The extended global progress state -->
                            <div class="progress-extended">&nbsp;</div>
                        </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <div role="presentation" class="table files">

                    </div>
                </form>
            </div>

            
            <li id="sentClone" class="msg-tbox msg-tbox-sent rfloat display-none">
                <a class="msgDelete msg-btn-delete">x</a>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/admin.png">
                <p class="msgSwitch"></p>
                <ul class="msgSwitch display-none">
                    <li>Elimina? 
                        <span class="noDelete btn btn-primary btn-mini">No</span> 
                        <span id="" class="yesDelete btn btn-danger btn-mini">Si</span>
                    </li>
                    <li class="display-none">
                        <a class="cancelDelete c-red" id="" href="javascript:;">Annulla</a>
                    </li>
                </ul>
                <span class="date-time">Adesso</span>
            </li>
            <li id="receivedClone" class="msg-tbox msg-tbox-received lfloat display-none">
                <a class="msgDelete msg-btn-delete">x</a>
                <img src="<?php echo HTTP_CATALOG; ?>image/message/user.png">
                <p class="msgSwitch"></p>
                <ul class="msgSwitch display-none">
                    <li>Elimina? 
                        <span class="noDelete btn btn-primary btn-mini">No</span> 
                        <span id="" class="yesDelete btn btn-danger btn-mini">Si</span>
                    </li>
                    <li class="display-none"><a class="cancelDelete c-red" id="" href="javascript:;">Annulla</a></li>
                </ul>
                <span class="date-time">Adesso</span>
            </li>
        </div>
                
        <div class="clearfix"></div>
  </div>
  </div>
</div>



<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-process.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-ui.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
    <script src="<?php echo HTTP_CATALOG; ?>catalog/view/javascript/blueimpup/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->


<?php echo $footer; ?>