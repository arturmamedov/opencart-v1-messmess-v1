<?php
// attenzione ai segnaposti, vengono cambiati in reward.php /catalog/controller/account
$_['mail_text'] = "Ciao %s hai un nuvo messaggio sul sito: <a href='http://www.penne-stampate.it/index.php?route=account/register'>www.penne-stampate.it</a> 
Messaggio <strong>'%s'</strong> 

Clicca sulla messaggistica per leggere.
<a href='http://www.penne-stampate.it/index.php?route=message/message'>Messagistica (1)</a>


Ai sensi del D.lgs n. 196 del 30.06.03 (Codice Privacy) si precisa che le informazioni contenute in questo messaggio sono riservate e ad uso esclusivo del destinatario.
";

$_['mail_subject'] = 'Nuovo messaggio su penne-stampate.it';

// Example field added (see related part in admin/controller/module/my_module.php)
$_['my_module_example'] = 'Messagistica - admin e user';



// Heading Goes here:
$_['heading_title']    = 'Messagistica';


// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module My Module!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_example']       = 'Example Entry:'; // this will be pulled through to the controller, then made available to be displayed in the view.
$_['entry_image']        = 'Image (WxH):';
$_['entry_limit']        = 'Limit:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module My Module!';
?>