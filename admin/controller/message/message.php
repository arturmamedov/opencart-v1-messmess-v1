<?php
/**
 * Admin Message Controller
 * @version 1.0  10/03/2014
 * @author arturmamedov1993@gmail.com
 */
class ControllerMessageMessage extends Controller {
	
	private $error = array(); 
	
	public function index() {
		//Load the language file for this module
		$this->load->language('message/message');

		//Set the title from the language file $_['heading_title'] string
		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['heading_title'] = $this->language->get('heading_title');
        // css
        $this->document->addStyle(HTTP_CATALOG.'/catalog/view/theme/default/stylesheet/message.css');
        $this->document->addStyle(HTTP_CATALOG.'/catalog/view/theme/default/stylesheet/emoticons.css');
        // js
        $this->document->addScript(HTTP_CATALOG.'/catalog/view/javascript/message.js');
        $this->document->addScript(HTTP_CATALOG.'/catalog/view/javascript/emoticons.js');
        
		
		//SET UP BREADCRUMB TRAIL. YOU WILL NOT NEED TO MODIFY THIS UNLESS YOU CHANGE YOUR MODULE NAME.
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('message/message', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('message/message/sendmessage', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');

        $this->load->model('message/message');
        $this->load->model('sale/customer');
        
		// how we order the conversation
		$this->data['order_last'] = "";
		$this->data['order_first'] = "";
		$this->data['order_az'] = "";
		$this->data['order_za'] = "";
		$order_cnv = (isset($this->request->get['order'])) ? $this->request->get['order'] : '';
		if($order_cnv == 'first'){
			$order_option = 'ts_last ASC';
			$this->data['order_first'] = "msg-current";
		} elseif($order_cnv == 'az'){
			$order_option = 'ASC';
			$this->data['order_az'] = "msg-current";
		} elseif($order_cnv == 'za'){
			$order_option = 'DESC';
			$this->data['order_za'] = "msg-current";
		} else {
			$order_option = 'ts_last DESC';
			$this->data['order_last'] = "msg-current";
		}
		
		// For frontend user we get only one conversation (with admin)
		$this->data['path_inbox'] = "";
		$this->data['path_all'] = "";
		$this->data['path_trash'] = "";
		$path = (isset($this->request->get['path'])) ? $this->request->get['path'] : '';
		if($path == 'trash') {
			$cnvs = $this->model_message_message->getCnv($this->model_message_message->getOtherUserId(), array('path' => 'TRASH', 'order' => $order_option));
			$this->data['path_trash'] = "msg-current";
		// all conversations that get a costumer and after attach the conversation to him
		} 
		elseif($path == 'all') {
			$users = $this->model_sale_customer->getCustomers(array('order' => $order_option));
			
			$all_cnvs = $this->model_message_message->getCnv($this->model_message_message->getOtherUserId());
			
			$fake_cnvs = array(
				'id' => 0,
				'user_one_id' => 0,
				'user_two_id' => 0,
				'one_last_active' => 'Mai',
				'two_last_active' => 'Mai',
				'one_status' => 0,
				'two_status' => 0,
				'one_path' => 'INBOX',
				'two_path' => 'INBOX',
				'ts_last' => 'Mai',
				'message' => 'Nessun messaggio'
			);
			
			$uid_cnvs = array();
			foreach($all_cnvs as $cnv){
				if($cnv['user_one_id'] == 1){
					$uid_cnvs[$cnv['user_two_id']] = $cnv;
				} else {
					$uid_cnvs[$cnv['user_one_id']] = $cnv;
				}
			}
			
			$cnvs = array();
			foreach($users as $user){
				if(array_key_exists($user['customer_id'], $uid_cnvs)){
					$cnvs[] = array_merge($user, $uid_cnvs[$user['customer_id']]);
				} else {
					$cnvs[] = array_merge($user, $fake_cnvs);
				}
			}
			
			if($order_option == 'ts_last ASC' || $order_option == 'ts_last DESC'){
				// Ordering Feedbacks array by ts_last more/less recent
				$ordered_cnvs = array();
				$last_tss = array();
				$kss = 1;
				$iss = 1;

				foreach($cnvs as $cnv){
					if($cnv['ts_last'] == NULL || $cnv['ts_last'] == 'Mai'){
						$ordered_cnvs[0 + $iss] = $cnv;
					} else {
						$format = 'Y-m-d H:i:s';
						$date = DateTime::createFromFormat($format, $cnv['ts_last']);            
						$ts_last = $date->format('U');
						
						if(array_key_exists($ts_last, $last_tss)){
							$ordered_cnvs[$ts_last + $kss] = $cnv;
							$kss++;            
						} else {
							$ordered_cnvs[$ts_last] = $cnv;
						}
						$last_tss[$ts_last] = '';
					}
					$iss++;
				}
				
				if($order_option == 'ts_last DESC'){
					krsort($ordered_cnvs);
				} else {
					ksort($ordered_cnvs);
				}
				
				$cnvs = $ordered_cnvs;
			}
			
			$this->data['conversations'] = $cnvs;
			$this->data['path_all'] = "msg-current";
		} 
		else {
			$cnvs = $this->model_message_message->getCnv($this->model_message_message->getOtherUserId(), array('path' => 'INBOX', 'order' => $order_option));
			
			$this->data['path_inbox'] = "msg-current";
		}
		$this->data['message_path'] = $path; 
        // and we select only the messages from that conversation
        $this->data['messages'] = array(); //$this->model_message_message->getMessages($this->data['conversations'][0]['id']);
        
        
        // default value
        $this->data['other_user_id'] = 0;
        //put some data to view
        $this->data['cnv_id'] = 0;
        
        // if no all users page, load data into array with customer_id at index
        if($path != 'all'){			
			$users_cnvs = array();
			foreach($cnvs as $cnv){
				if($cnv['user_one_id'] == 1){
					$cnv['other_user_id'] = $cnv['user_two_id'];
					$cnv['cnv_status'] = $cnv['one_status'];
					$users_cnvs[] = array_merge($cnv, $this->model_sale_customer->getCustomer($cnv['user_two_id']));
				} else {
					$cnv['other_user_id'] = $cnv['user_one_id'];
					$cnv['cnv_status'] = $cnv['two_status'];
					$users_cnvs[] = array_merge($cnv, $this->model_sale_customer->getCustomer($cnv['user_one_id']));
				}
			}
			
			if($order_option == 'ASC' || $order_option == 'DESC'){
				// Ordering Feedbacks array by firstname DESC/ASC
				$ordered_cnvs = array();
				$last_tss = array();

				foreach($users_cnvs as $cnv){
					$ordered_cnvs[$cnv['firstname']] = $cnv;
				}

				if($order_option == 'DESC'){
					krsort($ordered_cnvs);
				} else {
					ksort($ordered_cnvs);
				}

				$users_cnvs = $ordered_cnvs;
			}
			$this->data['conversations'] = $users_cnvs;
        }		
        // carichiamo il tamplate e mandiamo in output
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		//Choose which template file will be used to display this request.
		$this->template = 'message/message.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		//Send the output.
		$this->response->setOutput($this->render());
	}
    
	
    /**
     * Send message to conversation
     */
    public function sendmessage()
    {   
        $errors = array();
        if($this->request->server['REQUEST_METHOD'] == 'POST'){
            $this->load->model('message/message');
            
            // not used
            $cnv_id = $this->request->post['cnv_id'];
            if($cnv_id <= 0){
                $errors[] = "Manca id conversazione :(";
            }
            
            // important - other user id from html (setted in/on getmessage)
            $user_id = $this->request->post['user_id'];
            if($user_id <= 0){
                $errors[] = "Manca id utente :(";
            }
            $msgtxt = $this->request->post['msgtxt'];
            if(strlen($msgtxt) == 0){
                $errors[] = 'Prova a scrivere di piú :)';
            }
			$type = (isset($this->request->post['type'])) ? $this->request->post['type'] : 'text' ;
            $title = (isset($this->request->post['title'])) ? $this->request->post['title'] : null ;
			
            if(count($errors) > 0){
                if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                    echo json_encode(array('error' => 'error', 'error' => $errors[0], 'btn_text' => 'Riprova..'));
                    return;
                }
                $this->redirect($this->url->link('message/message', 'error=' . $errors[0]));
            }
                
            // load conversation that are loaded or created in index Action
            $this->model_message_message->loadByUsers($user_id, $this->model_message_message->getOtherUserId());

            $msgid = $this->model_message_message->createMessage($this->model_message_message->getOtherUserId(), $msgtxt, $type, $title);
            
            if($msgid <= 0){
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo json_encode(array('error' => "Errore inaspettato, per favore riprova ricaricando la pagina", 'btext' => 'Ricarica..'));
                return;
            }
            $this->redirect($this->url->link('message/message', "error=Errore inaspettato, per favore riprova ricaricando la pagina"));
            }
			if($type == 'text'){
				$msgtxt = $this->model_message_message->convertTextToLink(nl2br($msgtxt));
			}
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo json_encode(array('success' => 'success', 'msgtxt' => $msgtxt, 'msgid' => $msgid, 'cnvtxt' => substr($msgtxt, 0, 45), 'btext' => 'Invia', 'ts' => date('m-d H:i:s'), 'msgtitle' => $title, 'msgtype' => $type));
                return;
             }
            $this->redirect($this->url->link('message/message', 'success=Messaggio inviato con successo'));
        } else {
            $this->redirect($this->url->link('messag/message'));
        }
    }
    
    public function getmessages() {
        $this->load->model('message/message');

        $user_id = (isset($this->request->get['user_id'])) ? $this->request->get['user_id'] : null;
        $other_user_id = $this->model_message_message->getOtherUserId();            
        if($user_id == null){
            json_encode(array('error' => 'Ricarica la pagina e riprova'));
        } 
            
        $this->model_message_message->loadByUsers($user_id, $other_user_id);
        
        // set read messages
        $this->model_message_message->setRead($this->model_message_message->getOtherUserId());
        // and we select only the messages from that conversation
        $messages = $this->model_message_message->convertMessages($this->model_message_message->getMessages($this->model_message_message->id));
        
        $this->data['messages'] = $messages;
        // not used - user_id of second user setted via conversations.tpl html from attr
        $this->data['other_user_id'] = $this->model_message_message->getOtherUserId();
		
        $this->template = 'message/libs/messages.tpl';
        
        echo json_encode(array('success' => 'success', 'html' => $this->render(), 'cnv_id' => $this->model_message_message->id));
        return;
	}
	
	/**
     * Send message to conversation
     */
    public function listen()
    {   
		session_write_close();
		set_time_limit(0);
        $errors = array();
        
		$this->load->model('message/message');
        
		// what timestamp take for control if newMessages()
		$dt_timestamp = strtotime("-1 minutes");
        
		// loop that work for 40seconds after close connection and wait client loop to rerequest
		$i=0;
		do{
			//sleep(1);
			if($i > 20){
				echo json_encode(array('s' => 'nothing'));
				return;
			}
			$i++;
			$res = $this->model_message_message->newMessages($this->model_message_message->getOtherUserId(), $dt_timestamp);
		}while(!$res);
		
		$data['success'] = 'success';
		$data['event'] = 'new_msg';
		
		foreach($res as $msg){
			$this->model_message_message->load($msg['cnv_id']);
			$this->model_message_message->setRead($this->model_message_message->getOtherUserId());
            
			if($msg['type'] == 'text'){
				$msgtxt = $this->model_message_message->convertTextToLink(nl2br($msg['message']));
			} else {
				$msgtxt = $msg['message'];
			}
			
			$data['msgs'][] = array('cnv_id' => $msg['cnv_id'], 
					'nmsgs' => 1, 
					'msgtitle' => $msg['title'], 
					'msgtxt' => $msgtxt,
					'cnvtxt' => substr($msg['message'],0,45), 
					'msgid' => $msg['id'],
					'user_id' => $this->model_message_message->getOtherUserId(),
					'ts' => $msg['ts_created'],
					'msgtype' => $msg['type']
			);
		}
		
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			echo json_encode($data);
			return;
		} else {
            echo json_encode(array('error' => 'error'));
			return;
		}
    }
    
    public function totrash() {
        $this->load->model('message/message');
        $cnv_id = $this->request->get['cnv_id'];
        
        if($cnv_id == 0){
            json_encode(array('error' => 'Ricarica la pagina e riprova')); // @todo @totranslate
            return;
        }
        
		$this->model_message_message->load($cnv_id);
        
        if($this->model_message_message->one_user_id == $this->model_message_message->getOtherUserId()){
            $this->model_message_message->one_path = 'TRASH';
        } else {
            $this->model_message_message->two_path = 'TRASH';
        }
        
        $this->model_message_message->save();
        
        echo json_encode(array('success' => 'success'));
        return;
	}
    
    public function deletemessage() {
        $this->load->model('message/message');
        $msgid = $this->request->get['msgid'];
        
        if($msgid == 0){
            json_encode(array('error' => 'Ricarica la pagina e riprova')); // @todo @totranslate
            return;
        }
        
		$this->model_message_message->deleteMessage($msgid);
        
        echo json_encode(array('success' => 'success'));
        return;
	}
    
    public function undeletemessage() {
        $this->load->model('message/message');
        $msgid = $this->request->get['msgid'];
        
        if($msgid == 0){
            json_encode(array('error' => 'Ricarica la pagina e riprova')); // @todo @totranslate
            return;
        }
        
		$this->model_message_message->undeleteMessage($msgid);
        
        echo json_encode(array('success' => 'success'));
        return;
	}
}
?>