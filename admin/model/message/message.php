<?php
/**
 * Admin Message Model
 * @version 1.0  10/03/2014
 * @author arturmamedov1993@gmail.com
 */
class ModelMessageMessage extends Model {
    
	/**
     * Variabile del amministratore che riceverà tutti i messaggi
     * @var integer
     */
	private $other_user_id = 1;
    /**
     * Name of conversations table
     * @var string defined in constructor
     */
    private $_table;
    /**
     * Name of messages table
     * @var string defined in constructor
     */
    private $_messages_table;
    /**
     * Timestamp of time limit for send mail notif
     * @var string defined in constructor
     */
    public $send_mail_ts;

    public function __construct($registry){
        parent::__construct($registry);
        $this->_table = DB_PREFIX ."conversations";
        $this->_messages_table = DB_PREFIX . "conversations_messages";
        // timestamp
        $this->send_mail_ts = strtotime("-5 minutes");
    }
    
    
    /**
     * Properieta admin_id
     * 
     * @return integer
     */
    public function getOtherUserId(){
        return $this->other_user_id;
    }
    
    
    /** 
     * Get conversation for user
     * for frontend only one cnv
     * 
     * @param int $user_id
     * @param array $options
     *  
     * @return array
     */
    public function getCnv($user_id, $options = array()){
        // initialize the options
        $defaults = array('path' => 'ALL', 'status' => null, 'ts_last' => null);

        foreach($defaults as $k => $v) {
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        $query = "SELECT id, user_one_id, user_two_id, one_last_active, two_last_active, one_status, two_status, one_path, two_path, ts_last, (SELECT message FROM " . DB_PREFIX . "conversations_messages as cnvm WHERE cnvm.cnv_id=cnv.id LIMIT 1) as message FROM {$this->_table} as cnv ";
        
        if($options['path'] == 'ALL'){
            $query .= "WHERE user_one_id={$user_id} OR user_two_id={$user_id} ";
        } else {
            $query .= "WHERE (user_one_id={$user_id} AND one_path='{$options['path']}')"
                    . " OR (user_two_id={$user_id}  AND two_path='{$options['path']}') ";
        }
		
		if(isset($options['order']) && ($options['order'] == 'ts_last DESC' || $options['order'] == 'ts_last ASC')){
			$query .= 'ORDER BY '.$options['order'].' ';
		}
		$result = $this->db->query($query);
		
        return $result->rows;
    }
    
    
    /** 
     * Get conversation for user
     * for frontend only one cnv
     * 
     * @param int $user_id
     * 
     * @return array
     */
    public function getMessages($cnv_id){
        $query = "SELECT * FROM ".DB_PREFIX . "conversations_messages as cnvm WHERE cnv_id={$cnv_id} ORDER BY id ASC";

		$result = $this->db->query($query);
		
        return $result->rows;
    }
    

	/**
	 * Static function for convert text/html by passing options array
	 * 
	 * @param string $text
	 * @param array $options (if target-blank to false): link havent the target=_blank
	 *  
	 * @return string The converte string
	 */
    static public function convertTextToLink($text, $options = array()){
        //* initialize the default filters
        $defaults = array('target-blank' => true);
        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        /* Url link */
        if($options['target-blank']){
            // Find and replace all http/https links that are not part of an existing html anchor
            // Find and replace all naked www.links.com (without http://)
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3" target="_blank">$1$3</a>', $text);//@todo pass on a mind page for analytics and more security
        } else 
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3">$1$3</a>', $text); // @todo pass on a mind page for analytics and more security
        
        return $text;
    }
	
	
    /** 
     * Convert messages to readable
     * 
     * @param array $rows
     * 
     * @return array Converted
     */
    public function convertMessages($rows){
        foreach($rows as $key => $msg){
			if($rows[$key]['type'] == 'text'){
				// for new line in messages
				$rows[$key]['message'] = nl2br($msg['message']);
				$rows[$key]['message'] = self::convertTextToLink($msg['message']);
			}
            
            //$date = new DateTime($msg['ts_created']);
            $rows[$key]['rd_created'] = date("d  H:i:s", strtotime($msg['ts_created'])); //$date->format("d  H:i:s");
            $rows[$key]['rdf_created'] = date("d-m-Y  H:i:s", strtotime($msg['ts_created'])); //$date->format("d-m-Y  H:i:s");
        }
        return $rows;
    }
	
	/** 
     * Get only new messages for updates
     *
	 * @param int $user_id to which user id
     * @param int $dt_timestamp timestamp from that compare
     * 
     * @return array
     */
    public function newMessages($user_id, $dt_timestamp){
        $query = "SELECT * FROM {$this->_messages_table} as cnvm WHERE to_user_id = {$user_id} AND ts_created > '{$dt_timestamp}' AND one_active = 1 AND two_active = 1 AND status = 1  ORDER BY id ASC";
		$result = $this->db->query($query);
        
		$rows = $result->rows;
		
        if(count($rows) > 0){
			return $rows;
		} else {
			return false;
		}
    }

	/** 
     * Get the last image from order or messages by admin
     * #REPEATED FROM CATALOG .... with switch from HTTP_SERVER to HTTP_CATALOG 
     * @return array message array data
     */
    public function lastimgMessage(){
		$img_message = array(); // the message with image from cnv or order
		
		if($this->user_one_id == 1){
			$user_id = $this->user_two_id;
		} else {
			$user_id = $this->user_one_id;
		}
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.customer_id = '" . (int)$user_id . "' ORDER BY order_id DESC LIMIT 1");

		// get last img message
		$query = "SELECT * FROM {$this->_messages_table} as cnvm WHERE cnv_id = {$this->id} AND type = 'file-img' AND one_active = 1 AND two_active = 1 AND user_id = 1 ORDER BY id DESC LIMIT 1";
		$result = $this->db->query($query);
		$cnv_last_img = $result->row;
		
		// you can use @date_added or @date_modified
		if(isset($order_query->row['date_modified'])){
			$order_date = $order_query->row['date_modified'];
		} else {
			$order_date = 0;
		}
		if(isset($order_query->row['date_added']) && ($order_date == null || $order_date == '0000-00-00 00:00:00')){
			$order_date = $order_query->row['date_added'];
		}
		
		if($order_query->num_rows != 0 && (count($cnv_last_img) == 0 || $order_date > @$cnv_last_img['ts_created'])){
			$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_query->row['order_id'] . "'");
			
			foreach($order_option_query->rows as $opt){
				if($opt['order_option_id'] == 40 || $opt['type'] == 'file'){
					
					$file = DIR_DOWNLOAD . $opt['value'];
					$mask = basename(utf8_substr($opt['value'], 0, utf8_strrpos($opt['value'], '.')));
					$order_img_name = ($mask ? $mask : basename($file));
					
					$img_message = $this->fake_msg;
					$img_message['title'] = 'Bozzetto: '.$order_img_name;
					$img_message['message'] = HTTP_CATALOG . 'download/'.$opt['value'];
					$img_message['type'] = 'img-file';
					
					$img_message['ts_created'] = $order_query->row['date_added'];
					//$date = new DateTime($msg['ts_created']);
					$img_message['rd_created'] = date("d  H:i:s", strtotime($img_message['ts_created'])); //$date->format("d  H:i:s");
					$img_message['rdf_created'] = date("d-m-Y  H:i:s", strtotime($img_message['ts_created'])); //$date->format("d-m-Y  H:i:s");
				}
			}
		} elseif(count($cnv_last_img)) {
			// use $cnv_last_img
			$rows = $this->convertMessages(array($cnv_last_img));
			$img_message = $rows[0]; 
		} else {
			return false;
		}
		
		// return the last image from order or messages by admin
		return $img_message;
    }
    
    /** 
     * Get messages count
     * new, all and for 
     * 
     * @param int $user_id
     * 
     * @return array
     */
    public function getMessagesCount($user_id, $what = 'new'){
        switch($what){
            case'new':
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id} AND status = 1";
            break;
            case'all':
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id} AND one_active = 1 AND two_active = 1";
            break;
            default:
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id} AND status = 1";
            break;
        }
            
        $result = $this->db->query($query);
        return $result->row['count(*)'];
    }
    
    /** 
     * Load and set data to object, from db = cnv_id in $row
     * or from array 
     * 
     * @param array/int $row
     * 
     * @return array
     */
    public function load($row){
        if(is_array($row)){
            $this->id = $row['id'];
            $this->user_one_id = $row['user_one_id'];
            $this->user_two_id = $row['user_two_id'];
            $this->one_last_active = $row['one_last_active'];
            $this->two_last_active = $row['two_last_active'];
            $this->one_path = $row['one_path'];
            $this->two_path = $row['two_path'];
            $this->ts_last = $row['ts_last'];
        } elseif($row > 0) {
            $query = sprintf("SELECT * FROM %s WHERE id = {$row}", $this->_table);

            $result = $this->db->query($query);
            $row = $result->row;

            $this->load($row);
        }
        
        return true;
    }
    
    /**
     * Load conversation by the two users
     * or create them
     * @param int $user_one_id
     * @param int $user_two_id
     * 
     * @return boolean
     * 
     * @throws Exception
     */
    public function loadByUsers($user_one_id, $user_two_id){
		if($user_one_id <= 0 || $user_two_id <= 0){
			return false;
		}
		
        $query = sprintf("SELECT * FROM %s WHERE (user_one_id = '{$user_one_id}' AND user_two_id = '{$user_two_id}') OR (user_one_id = '{$user_two_id}' AND user_two_id = '{$user_one_id}')", $this->_table);

        $result = $this->db->query($query);
        // ricevere in array associativo
        if(!$row = $result->row){
            $row = $this->createConversation($user_one_id, $user_two_id, 0);
        }
        $this->load($row);
        
        return $row;
        //return $this->_load($query);
    }
    
    /**
     * Save data of conversation
     * 
     * @return boolean
     */
    public function save(){
        $query = sprintf("
            UPDATE {$this->_table} SET  
                    `one_last_active` = '%s', 
                    `two_last_active` = '%s',
                    `one_status` = %d, 
                    `two_status` = %d,
                    `one_path` = '%s',
                    `two_path` = '%s',
                    `ts_last` = '%s'
            WHERE id = {$this->id}",
        $this->one_last_active, $this->two_last_active, $this->one_status, $this->two_status, $this->one_path, $this->two_path, $this->ts_last);
		
        $this->db->query($query);
        
        return true;
    }
       
    /**
     * Create conversation on first send
     * 
     * @param int $user_one_id
     * @param int $user_two_id
     * @param int $status 
     * @return boolean
     * 
     * @throws Exception
     */
    public function createConversation($user_one_id, $user_two_id, $status = 1){
		if($user_one_id <= 0 || $user_two_id <= 0){
			return false;
		}
	
        $this->db->query("INSERT INTO `{$this->_table}` (
                `id`,
                `user_one_id` ,
                `user_two_id` ,
                `two_status`
        ) VALUES (
            NULL , '{$user_one_id}', '{$user_two_id}', '{$status}'
        )");
        
        return $this->db->getLastId();
    }
  
    /**
     * Create message on send
     * 
     * @param int $cnv_id
     * @param int $user_id // from user id
     * @param int $other_user_id // to user id
     * @param string $message
     * @param string $type
     * @param string $title
	 *
     * @return boolean
     * 
     * @throws Exception
     */
    public function createMessage($user_id, $message, $type, $title = null){
        $message = addslashes($message);
        
        if($this->user_one_id == $user_id){
            $other_user_id = $this->user_two_id;
            $this->two_status = 1;
            $this->one_last_active = date('Y-m-d H:i:s');
            $other_online = (strtotime($this->two_last_active) < $this->send_mail_ts) ? false : true;
        } elseif($this->user_two_id == $user_id){
            $other_user_id = $this->user_one_id;
            $this->one_status = 1;
            $this->two_last_active = date('Y-m-d H:i:s');
            $other_online = (strtotime($this->one_last_active) < $this->send_mail_ts) ? false : true;
        } else {
            return 0; // false
        }
        
        // quando creiamo il messaggio mettiamo la conversazione in INBOX
        $this->one_path = 'INBOX';
        $this->two_path = 'INBOX';
        
		$this->ts_last = date('Y-m-d H:i:s');
        $this->save();
        
        $this->db->query("INSERT INTO `{$this->_messages_table}` (
                `id`,
                `cnv_id`,
                `user_id` ,
                `to_user_id` ,
                `message`,
                `title`,
                `one_active`,
                `two_active`,
                `status`,
				`type`
        ) VALUES (
            NULL , {$this->id}, {$user_id}, {$other_user_id}, '{$message}', '{$title}', 1, 1, 1, '{$type}'
        )");
        $msgid = $this->db->getLastId();

        if(!$other_online){
            $this->sendMail($message, $other_user_id);
        }

        return $msgid;
    }
    
    
    /**
     * Send notif mail to user (new message received on site)
     * [called in $this->createMessage() if other user offline]
     * 
     * @param string $msgtxt text of new message received
     * @param int $user_id id of user that may receive message (perform action)
     * 
     * @return boolean true if send are ok
     */
    public function sendMail($msgtxt, $user_id){
		$this->language->load('message/message');

		$this->load->model('sale/customer');
		$customer = $this->model_sale_customer->getCustomer($this->getOtherUserId());
		$other_user = $this->model_sale_customer->getCustomer($user_id);
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($other_user['email']);
		$mail->setFrom($customer['email']);
		$mail->setSender($customer['firstname'].' '.$customer['lastname']);
			
		$mail->setSubject($this->language->get('mail_subject'));
		$mail->setText(strip_tags(html_entity_decode(sprintf($this->language->get('mail_text'), $other_user['firstname'].' '.$other_user['lastname'], $msgtxt), ENT_QUOTES, 'UTF-8')));
		$mail->send();

		return true;
	}
    
    
    /**
     * Set to 0 a conversation user status
     * 
     * @param int $user_id
     * 
     * @return boolean true/false if not setted
     */
    public function setRead($user_id){
        if($this->user_one_id == $user_id){
            $this->one_status = 0; // one , read
			$this->one_last_active = date('Y-m-d H:i:s');
        } elseif($this->user_two_id == $user_id) {
            $this->two_status = 0; // two, read
			$this->two_last_active = date('Y-m-d H:i:s');
        }
        
        // salva i nuovi dati
        $this->save();
        
        $this->setMessageRead($this->id, $user_id);
        
        return true;
    }
    
    
    /**
     * Set to 1 a conversation user status
     * 
     * @param int $user_id
     * 
     * @return boolean true/false if not setted
     */
    public function setUnRead($user_id){
        if($this->user_one_id == $user_id)
            $this->one_status = 1;
        elseif($this->user_two_id == $user_id)
            $this->two_status = 1;
        else
            return false;
        
        $this->save();
        return true;
    }
    
    
    
    /**
     * Set to 0 a messages user status
     * 
     * @param int $cnv_id
     * @param int $user_id
     * 
     * @return boolean true
     */
    public function setMessageRead($cnv_id, $user_id){
        $query = sprintf("UPDATE %s SET  `status` = '0' WHERE `status` = 1 AND cnv_id = %d AND to_user_id = %d", $this->_messages_table, $cnv_id, $user_id);
        // ORDER BY id DESC LIMIT 1; // set read all now

        $this->db->query($query);
        return $this->db->getLastId();
    }
    
    
    /**
     * Set to 0 both user active messages
     * 
     * @param int $msgid
     * 
     * @return boolean true
     */
    public function deleteMessage($msgid){
        $query = sprintf("UPDATE %s SET `one_active` = 0, `two_active` = 0 WHERE `id` = {$msgid}", $this->_messages_table);
        $this->db->query($query);
        return $this->db->getLastId();
    }
    
    /**
     * Set to 0 both user active messages
     * 
     * @param int $msgid
     * 
     * @return boolean true
     */
    public function undeleteMessage($msgid){
        $query = sprintf("UPDATE %s SET `one_active` = 1, `two_active` = 1 WHERE `id` = {$msgid}", $this->_messages_table);
        $this->db->query($query);
        return $this->db->getLastId();
    }
    
    /**
     * Set to 1 a messages user status
     * 
     * @param int $cnv_id
     * @param int $user_id (other user for unread)
     * @param int $user_num
     * 
     * @return boolean true
     * /
    public function setMessageUnRead($cnv_id, $user_id, $user_num){
        try{
            $last_msg = self::GetLastMessage($this->_db, $cnv_id, $user_id, 1, $user_num);
            
            $where = $this->_db->quoteInto("id = ?", $last_msg['id']);
        
            $this->_db->update('auayaya_messages', array('status' => 1), $where);
        } catch (Exception $e){
            throw new Exception('DBOMessage pf- setUnRead error with: '. $e->getMessage() .' | Trace: '. $e->getTraceAsString());
        }
        return true;
    } /* */



    // This function is how my blog module creates it's tables to store blog entries. You would call this function in your controller in a
    // function called install(). The install() function is called automatically by OC versions 1.4.9.x, and maybe 1.4.8.x when a module is
    // installed in admin.
    public function createModuleTables() {
        $query = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "conversations (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_one_id` int(11) NOT NULL,
            `user_two_id` int(11) NOT NULL,
            `one_last_active` tinyint(4) NOT NULL DEFAULT '1',
            `two_last_active` tinyint(4) NOT NULL DEFAULT '1',
            `one_status` tinyint(4) NOT NULL DEFAULT '0',
            `two_status` tinyint(4) NOT NULL DEFAULT '1',
            `one_path` varchar(20) NOT NULL DEFAULT 'INBOX',
            `two_path` varchar(20) NOT NULL DEFAULT 'INBOX',
            `ts_last` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
        ");

        $query = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "conversations_messages (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `cnv_id` bigint(20) NOT NULL,
            `user_id` bigint(20) NOT NULL,
            `to_user_id` bigint(20) NOT NULL,
            `message` text NOT NULL,
            `one_active` tinyint(4) NOT NULL DEFAULT '1',
            `two_active` tinyint(4) NOT NULL DEFAULT '1',
            `status` tinyint(4) NOT NULL DEFAULT '1',
            `ts_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;
        ");
        return true;
    }
}
?>