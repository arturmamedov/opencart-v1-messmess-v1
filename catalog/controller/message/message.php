<?php
/**
 * Frontend Message Controller
 * @version 1.0  10/03/2014
 * @author arturmamedov1993@gmail.com
 */
class ControllerMessageMessage extends Controller {
	public function index() {
        if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('message/message', '', 'SSL');
			$this->redirect($this->url->link('account/login', '', 'SSL'));
		}
        
		//Load the language file for this module - catalog/language/message/message.php
		$this->language->load('message/message');

		$this->document->setTitle($this->language->get('heading_title'));
        // css
        $this->document->addStyle('catalog/view/theme/default/stylesheet/message.css');
        $this->document->addStyle('catalog/view/theme/default/stylesheet/emoticons.css');
        // js
        $this->document->addScript('catalog/view/javascript/message.js');
        $this->document->addScript('catalog/view/javascript/emoticons.js');

		//* aggiungiamo i link sotto al header
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('message/message'),
			'separator' => $this->language->get('text_separator')
		);

		//Load any required model files - catalog/product is a common one, or you can make your own DB access
		//methods in catalog/model/message/message.php
		$this->load->model('message/message');

        //* TO VIEW DATA & LANG
		//Get the title from the language file
      	$this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['admin_name'] = $this->language->get('admin_name');

		// For frontend user we get only one conversation (with admin)
		$this->data['conversations'] = $this->model_message_message->loadByUsers($this->customer->getId(), $this->model_message_message->getOtherUserId());
        // and we select only the messages from that conversation
        $this->data['messages'] = $this->model_message_message->convertMessages($this->model_message_message->getMessages($this->model_message_message->id));
		
		$this->data['last_img'] = $this->model_message_message->lastimgMessage();
		
        // put some data to view
        $this->data['other_user_id'] = $this->model_message_message->getOtherUserId();
        $this->data['cnv_id'] = $this->model_message_message->id;
        
        if($this->model_message_message->user_one_id == $this->customer->getId()){
            $this->data['status'] = $this->model_message_message->one_status;
        } elseif($this->model_message_message->user_two_id == $this->customer->getId()){
            $this->data['status'] = $this->model_message_message->two_status;
        }
        
        // set read messages
        $this->model_message_message->setRead($this->customer->getId());
        
        // ERRORS
        if(isset($this->request->get['error'])){
            $this->data['error'] = $this->request->get['error'];
        } if(isset($this->request->get['success'])){
            $this->data['success'] = $this->request->get['success'];
        }
        $this->data['action'] = $this->url->link('message/message/sendmessage');
        
		//Choose which template to display this module with
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/message/message.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/message/message.tpl';
		} else {
			$this->template = 'default/template/message/message.tpl';
		}
        
		//* diciamo quali estensioni devono essere caricate
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		//Render the page with the chosen template
		$this->response->setOutput($this->render());
	}
    
    /**
     * Send message to conversation
     */
    public function sendmessage()
    {   
        $errors = array();
        if($this->request->server['REQUEST_METHOD'] == 'POST'){
            $this->load->model('message/message');
            
            // not used
            $cnv_id = $this->request->post['cnv_id'];
            if($cnv_id <= 0){
                $errors[] = "Errore inaspettato, per favore riprova ricaricando la pagina";
            }
            // not used
            $user_id = $this->request->post['user_id'];
            if($user_id != $this->model_message_message->getOtherUserId()){
                $errors[] = "Errore inaspettato, per favore riprova ricaricando la pagina";
            }
            $msgtxt = $this->request->post['msgtxt'];
            if(strlen($msgtxt) == 0){
                $errors[] = 'Prova a scrivere di piú :)';
            }
			$type = (isset($this->request->post['type'])) ? $this->request->post['type'] : 'text' ;
			$title = (isset($this->request->post['title'])) ? $this->request->post['title'] : null ;
            
            if(count($errors) > 0){
                if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                    echo json_encode(array('error' => $errors[0], 'btext' => 'Riprova..'));
                    return;
                }
                $this->redirect($this->url->link('message/message', 'error=' . $errors[0]));
            } 
            // load conversation that are loaded or created in index Action
            $this->model_message_message->loadByUsers($this->customer->getId(), $this->model_message_message->getOtherUserId());

            $msgid = $this->model_message_message->createMessage($this->customer->getId(), $msgtxt, $type, $title);
            
            if($msgid <= 0){
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo json_encode(array('error' => "Errore inaspettato, per favore riprova ricaricando la pagina", 'btext' => 'Ricarica..'));
                return;
            }
            $this->redirect($this->url->link('message/message', "error=Errore inaspettato, per favore riprova ricaricando la pagina"));
            }
			
			if($type == 'text'){
				$msgtxt = $this->model_message_message->convertTextToLink(nl2br($msgtxt));
            }
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo json_encode(array('success' => 'success', 'msgtxt' => $msgtxt, 'msgid' => $msgid, 'cnvtxt' => substr($msgtxt, 0, 45), 'btext' => 'Invia', 'ts' => date('m-d H:i:s'), 'msgtitle' => $title, 'msgtype' => $type));
                return;
             }
            $this->redirect($this->url->link('message/message', 'success=Messaggio inviato con successo'));
        } else {
            $this->redirect($this->url->link('messag/message'));
        }
    }
	
	/**
     * Send message to conversation
     */
    public function listen()
    {   
		session_write_close();
		set_time_limit(0);
        
		$this->load->model('message/message');
		
        // load conversations data
		$this->model_message_message->loadByUsers($this->customer->getId(), $this->model_message_message->getOtherUserId());
			
		// what timestamp take for control if newMessages()
		if($this->model_message_message->user_one_id == $this->customer->getId()){
			$dt_timestamp = $this->model_message_message->one_last_active;
		} elseif($this->model_message_message->user_two_id == $this->customer->getId()){
			$dt_timestamp = $this->model_message_message->two_last_active;
		} else {
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				echo json_encode(array('error' => 'error', 'error' => "Errore inaspettato, per favore riprova ricaricando la pagina"));
				return;
			} else {
				return;
			}
		}
		
		
		// loop that work for 40seconds after close connection and wait client loop to rerequest
		$i=0;
		do{
			//sleep(1);
			if($i > 20){
                /* we dont need this - setRead on receiveMessage is better
                if($this->model_message_message->user_one_id == $this->customer->getId()){
                    $this->model_message_message->one_last_active = date('Y-m-d H:i:s');
                    $this->model_message_message->save();
                } elseif($this->model_message_message->user_two_id == $this->customer->getId()){
                    $this->model_message_message->two_last_active = date('Y-m-d H:i:s');
                    $this->model_message_message->save();
                }*/
                
				echo json_encode(array('s' => 'nothing'));
				return;
			}
			$i++;
			$res = $this->model_message_message->newMessages($this->customer->getId(), $dt_timestamp);
		}while(!$res);
		
		$this->model_message_message->setRead($this->customer->getId());
		
		$data['success'] = 'success';
		$data['event'] = 'new_msg';
		
		foreach($res as $msg){
			if($msg['type'] == 'text'){
				$msgtxt = $this->model_message_message->convertTextToLink(nl2br($msg['message']));
			} else {
				$msgtxt = $msg['message'];
			}
			
			$data['msgs'][] = array('cnv_id' => $this->model_message_message->id, 
					'nmsgs' => 1, 
					'msgtitle' => $msg['title'], 
					'msgtxt' => $msgtxt,
					'cnvtxt' => substr($msg['message'],0,45),
					'msgid' => $msg['id'],
					'user_id' => $this->model_message_message->getOtherUserId(),
					'ts' => $msg['ts_created'],
					'msgtype' => $msg['type']
			);
		}
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			echo json_encode($data);
			return;
		} else {
            echo json_encode(array('error' => 'error'));
			return;
		}
    }
    
}
?>