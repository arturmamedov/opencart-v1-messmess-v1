$(window).load(function(){
/** **************************************************** ** /
 *      START of MESSAGES function/action/event :)     **
 */
// adapt chat to window height
$('.msg-chat', $('.withChat')).height($(window).height() - 300);

/**
 * On click to contact list
 * for call gotoConversations()
 * for set ToRead the conversation if are new
 */
$(".msg-users").on('click', ".msg-user", function(){
	if($(".msg-users.admin").length > 0){
		var uName = $(this).find("h2").text(), user_id = $(this).attr('uid'), user_ref = $(this).attr('uu'), cnv_id = $(this).attr('id');
		
		gotoConversation($(this), cnv_id, uName, user_id, user_ref, 0);
	} else {
		$(this).removeClass('new').addClass('selected');
    }
});

/**
 * For set a conversation and message how read
 * 
 * @param {string/int} cnv_id
 * 
 * @returns {undefined}
 * /
function setreadConversation(cnv_id){
	//var msguser = $("#"+cnv_id, $(".msg-users"));
	//if(msguser.hasClass('new')){
		//msguser.removeClass('new').addClass('selected');
	/*$.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/chat/setreadconversation',
        cache: 'false',
        data: {cnv_id: cnv_id},
        timeout: 20000,
        success: function(json){
            if(json.cnv_num == 0){
                $("#messageBar > a > span").removeClass('icon-message-small-active').addClass('icon-message-small-inactive');
                $("#messageBar > a > span > h6").removeClass('c-white ch-azure allert-txt').addClass('c-azure ch-white thin-text')
            }
            
            $("#messageBar > a > span > h6").text(json.cnv_num);
            $("#messageBox").removeClass('init');
        }
    });* /
}/* */

/**
 * Show loader, put mous in focus on form
 * Show loader
 * Put user name in header and set his online/offline status
 * call openConversation() for load prev users conversation messages if isset
 * 
 * @param {dom_object} html
 * @param {int} cnv_id
 * @param {string} uName
 * @param {int} user_id
 * @param {string} user_ref
 * @param {int} uOnoff
 * @returns {nothing}
 */
function gotoConversation(html, cnv_id, uName, user_id, user_ref, uOnoff){
    //$("#contactCnv", $(".msg-users-big")).hide();
    
    if(html.hasClass('selected'))
        return false;
    else {
        $(".msg-tbox", $(".msg-chat")).remove();

        $(".msg-user", $(".msg-users")).removeClass('selected');
        html.removeClass('new').addClass('selected');

        $(".newCnv #cnvOpt", $(".withChat")).show();

        $("textarea", $(".msg-write")).removeAttr('disabled');
        $("input[type=submit]", $(".msg-write")).removeAttr('disabled');
        $('input[name=user_id]', $(".msg-write")).val(user_id);
        $('input[name=cnv_id]', $(".msg-write")).val(cnv_id);
        $("textarea", $(".msg-write")).val('').focus();

        openConversation(html, cnv_id, user_id);
    }
}

/**
 * For load prev users conversation messages if isset
 * 
 * @param {domObj} cnvObj
 * @param {int} cnv_id
 * 
 * @returns {nothing}
 */
function openConversation(cnvObj, cnv_id, user_id){
    var thisBox = $(".msg-chat");
    
    $("#msgError").hide();
    $("#msgLoader").show(); // .css('bottom', '-200px')

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: cnvObj.attr('data-url'),
        data: {cnv_id: cnv_id, user_id: user_id},
        cache: 'false',
        timeout: 20000,
        success: function(json){
            if(json.success){
                if(cnv_id == 0){
                    gotoConversation(cnvObj, json.cnv_id, '', user_id, '', 0);
                }
                $("#msgLoader").hide(); //.css('top', '0px')
                $("#msgError").hide();

                thisBox.prepend(json.html);
                
                downScroll($(".msg-chat"));
                //$('.msg-user-numNew', cnvObj).text('');
                
                $('.msg-tbox p').emoticonize({
                    delay: 1500,
                    animate: false
                    //exclude: 'pre, code, .no-emoticons'
                });
				
				$(".dropzone").show();
            } else if(json.error) {
                $("#msgError").show().text(json.error);
                setTimeout(function(){$("#msgError").fadeOut('slow');},5000);
            }               
        },
        error: function(){
            $("#msgError").show().text('Unaxpected Error :(');
            setTimeout(function(){$("#msgError").fadeOut('slow');},5000);
        }
    });
    
}

/**
 * On send new messages
 * capture keyboard input and call sendMessage() on Enter
 */
$(".msg-write").on('keydown', "textarea", function(e){
    //$(this).emoticonize({animate: false, delay: 1500});
    if(e.keyCode == 13 && !e.shiftKey){
        e.preventDefault();
        var thisForm = $(this).parent(); // la form da inviare
        sendMessage(thisForm, $(this).val(), 'text', '');
        return false;
    }
});

/**
 * Send new messages on submit of form with sendMessage()
 */
$(".msg-write").on('submit', 'form', function(e){
    e.preventDefault();
    var msgtxt = $(this).find('textarea').val();
    sendMessage($(this), msgtxt, 'text', '');
    return false;
});

/**
 * Send a message to other user of conversation
 * show loader on button
 * insert new message if success
 * show error and change button to try-again if error
 * 
 * @param {dom_object} thisForm
 * @param {string} msgtxt
 * 
 * @returns {nothing}
 */
function sendMessage(thisForm, msgtxt, type, title){
    // disable button for no re-press
    $("input[type=submit]", thisForm).attr('disabled', 'disabled');
    
    var user_id = thisForm.find('input[name=user_id]').val(), cnv_id = thisForm.find('input[name=cnv_id]').val();
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: thisForm.attr('action'),
        data: {msgtxt: msgtxt, user_id: user_id, cnv_id: cnv_id, type: type, title: title},
        cache: 'false',
        timeout: 20000,
        success: function(json){
            if(json.success){
                //$(".loader", thisForm).hide();
                
                if($(".withChat").length > 0)
                    addMsgChat(thisForm, cnv_id, json);
                else {
                    thisForm.find('textarea').val('');
                    msgDialog.find('.divSwitch').toggle();
                    setTimeout(function(){msgDialog.dialog('close'); msgDialog.find('.divSwitch').toggle(); }, 2000);
                }
            } else if(json.error) {
                //$(".loader", thisForm).hide();
                $("input[type=submit]", thisForm).removeClass('btn-primary').addClass('btn-warning');
                
                $("#msgError").show().text(json.error);
                setTimeout(function(){$("#msgError").fadeOut('slow');},5000);
            }
            
            $("input[type=submit]", thisForm).removeAttr('disabled').val(json.btext);
        },
        error: function(){
            //$(".loader", thisForm).hide();
            $("input[type=submit]", thisForm).val('Error').removeClass('btn-primary').addClass('btn-warning').removeAttr('disabled');
        }
    });
}

function addMsgChat(thisForm, cnv_id, json){
    if($("#"+json.msgid, $(".msg-chat")).length == 0){
		var sentClone = $("#sentClone", $(".msg-write")).clone();
		
		
		$("input[type=submit]", thisForm).val(json.btext).removeClass('btn-warning').addClass('btn-primary');
					
		if(json.msgtype == 'file'){
			var html = '<a href="'+json.msgtxt+'" target="_blank"><div class="preview docfile"><img class="lfloat" src="../../image/docfile.png"></div>'+json.msgtitle+'</a>';
			sentClone.find('p').html(html);
		} else if(json.msgtype == 'file-img') {
			var html = '<a href="'+json.msgtxt+'" target="_blank"><div class="preview"><img class="lfloat" src="'+json.msgtxt+'"></div>'+json.msgtitle+'</a>';
			sentClone.find('p').html(html);
		} else {
			sentClone.find('p').html(json.msgtxt);
		}
		sentClone.find('.yesDelete').attr('id', json.msgid);
		sentClone.find('.cancelDelete').attr('id', json.msgid);
		sentClone.attr('title', json.ts);
		sentClone.addClass('chat-lnew').removeClass('display-none');

		$(thisForm).find('textarea').val('');
		//$('#'+cnv_id, $(".msg-users")).find('p').text(json.cnvtxt);
		sentClone.attr('id', json.msgid);
		sentClone.show().appendTo($(".msg-chat"));
		
		downScroll($(".msg-chat"));

		// @maragliata
		sentClone.animate({backgroundColor: '#FFFFFF',}, 2500 );
		$('p', sentClone).emoticonize({animate: false, delay: 1500});
	}
}


// for scroll down and see last messages :)
function downScroll(element){
    $(element).scrollTop($(element)[0].scrollHeight);
}

/* * * * * * * * * * * * * * * *
 * Conversation Option BEGIN */
/**
 * For context option of conversation menu
 */
$(".newCnv", $(".withChat")).on('click', '#cnvOpt', function(){
    $("#cnvOptMenu .delete").next().hide();
    if($(this).next().hasClass('isset')){
        $(this).next().removeClass('isset');
        $(this).next().slideUp();
    } else {
        $(this).next().addClass('isset');     
        $(this).next().slideDown();
    }
    return false;
});
/**
 *  Set to unread one message and conversation
 */
$("#cnvOptMenu").on('click', '.setUnRead', function(){
    var user_id = $(".msg-write form").find('input[name=user_id]').val(), cnv_id = $(".msg-write form").find('input[name=cnv_id]').val();
    setunreadConversation(cnv_id, user_id);
    $(this).parent().slideUp();
    return false;
});
/**
 * Delete(toTrash) Yes/No 
 */
$("#cnvOptMenu").on('click', '.delete', function(){
    $(this).next().slideDown();
});      
$(".noDelete", $("#cnvOptMenu")).click(function(){
    //$("#cnvOptMenu").slideUp();
    $(this).parent().slideUp();
});
$(".yesDelete", $("#cnvOptMenu")).click(function(){            
    var user_id = $(".msg-write form").find('input[name=user_id]').val(), cnv_id = $(".msg-write form").find('input[name=cnv_id]').val(), url = $(this).attr('data-url');
    deleteConversation(cnv_id, user_id, url);
    $(this).parent().slideUp();
    $("#cnvOptMenu").slideUp();
});
/**
 * Definitive Delete Yes/No 
 */
$("#cnvOptMenu").on('click', '.defdelete', function(){
    $(this).next().slideDown();
});      
$(".noDelete", $("#cnvOptMenu")).click(function(){
    //$("#cnvOptMenu").slideUp();
    $(this).parent().slideUp();
});
$(".yesdDelete", $("#cnvOptMenu")).click(function(){
    var user_id = $(".msg-write form").find('input[name=user_id]').val(), cnv_id = $(".msg-write form").find('input[name=cnv_id]').val();
    defdeleteConversation(cnv_id, user_id);
    $(this).parent().slideUp();
    $("#cnvOptMenu").slideUp();
});
/**
 * Dont close menu on click to it elements
 */
$("#cnvOptMenu").mouseup(function(){return false;});
$(".noDelete", $("#cnvOptMenu")).mouseup(function(){return false;});
// and see in plugins for // @documentclick

/**
 * Delete a conversation but no the messages (because first all going to trash, after definive delete)
 * 
 * @param {int/string} cnv_id
 * @param {int/string} user_id
 * 
 * @returns {undefined}
 */
function deleteConversation(cnv_id, user_id, url){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: url,
        cache: 'false',
        data: {cnv_id: cnv_id, user_id: user_id},
        timeout: 20000,
        success: function(){
            // remove all chat messages
            $(".msg-tbox", $(".msg-chat")).remove();
            // clear and disable form for write
            $("textarea", $(".msg-write")).attr('disabled','disabled');
            $("input[type=submit]", $(".msg-write")).attr('disabled','disabled');
            $('input[name=user_id]', $(".msg-write")).val(' ');
            $('input[name=cnv_id]', $(".msg-write")).val(' ');
            /* clear header
            $(".newCnv #uName", $(".withChat")).text('');
            $(".newCnv #uName", $(".withChat")).attr('href', '');
            $(".newCnv #uOnoff", $(".withChat")).hide();
            $(".newCnv #cnvOpt", $(".withChat")).hide();*/
            // remove conversation
            $('#'+cnv_id, $(".msg-users")).remove();
        },
        error: function(){
            $("#msgError").show().text('Unaxpected Error :(');
            setTimeout(function(){$("#msgError").fadeOut('slow');},5000);
        }
    });
}

/**
 * Delete a conversation and all his messages
 * 
 * @param {int/string} cnv_id
 * @param {int/string} user_id
 * 
 * @returns {undefined}
 * /
function defdeleteConversation(cnv_id, user_id){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/chat/defdeleteconversation',
        cache: 'false',
        data: {cnv_id: cnv_id, user_id: user_id},
        timeout: 20000,
        success: function(){
            // remove all chat messages
            $(".msg-tbox", $(".msg-chat")).remove();
            // clear and disable form for write
            $("textarea", $(".msg-write")).attr('disabled','disabled');
            $("input[type=submit]", $(".msg-write")).attr('disabled','disabled');
            $('input[name=user_id]', $(".msg-write")).val(' ');
            $('input[name=cnv_id]', $(".msg-write")).val(' ');
            // clear header
            $(".newCnv #uName", $(".mjChat")).text('');
            $(".newCnv #uName", $(".mjChat")).attr('href', '');
            $(".newCnv #uOnoff", $(".mjChat")).hide();
            $(".newCnv #cnvOpt", $(".mjChat")).hide();
            // remove conversation
            $('#'+cnv_id, $(".msg-users-big")).remove();
        }
    });
}/* */

/* Conversation Option END *
/* * * * * * * * * * * * * */

/**
 * Set a conversation and his last message toUnRead (1)
 * 
 * @param {int/string} cnv_id
 * @param {int/string} user_id
 * 
 * @returns {undefined}
 * /
function setunreadConversation(cnv_id, user_id){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/chat/setunreadconversation',
        cache: 'false',
        data: {cnv_id: cnv_id, user_id: user_id},
        timeout: 20000,
        success: function(){
            $('#'+cnv_id, $(".msg-users-big")).removeClass('msg-user-selected').addClass('msg-user-new');
            $('#'+cnv_id+' .msg-user-numNew', $(".msg-users-big")).text('1');
            
            $("#messageBar > a > span").removeClass('icon-message-small-inactive').addClass('icon-message-small-active');
            $("#messageBar > a > span > h6").removeClass('c-azure ch-white thin-text').addClass('c-white ch-azure allert-txt').text(parseInt($("#messageBar > a > span > h6").text()) + 1);
            $("#messageBox").removeClass('init');
        }
    });
}/* */

/* * * * * * * * * 
 Message Delete */
/**
 * Definitive Delete Yes/No 
 */
$(".msg-chat").on('click', '.msgDelete', function(){
    $('.msgSwitch', $(this).parent()).toggle();
});
$(".msg-chat").on('dblclick', '.msg-tbox', function(){
    $('.msgSwitch', $(this)).toggle();
});
$(".msg-chat").on('click', '.noDelete', function(){
    $('.msgSwitch', $(this).parent().parent().parent()).toggle();
});
$(".msg-chat").on('click', '.yesDelete', function(){
    var user_id = $(".msg-write form").find('input[name=user_id]').val(), msgid = $(this).attr('id'), cnv_id = $(".msg-write form").find('input[name=cnv_id]').val();
    deleteMessage(cnv_id, msgid, user_id, $(this).parent().parent().parent());
});
$(".msg-chat").on('click', '.cancelDelete', function(){
    var user_id = $(".msg-write form").find('input[name=user_id]').val(), msgid = $(this).attr('id'), cnv_id = $(".msg-write form").find('input[name=cnv_id]').val();
    undeleteMessage(cnv_id, msgid, user_id, $(this).parent().parent().parent());
});

/**
 * Delete messages
 * 
 * @param {int/string} cnv_id
 * @param {int/string} msgid
 * @param {int/string} user_id
 * @param {dom/object} thisBox
 * 
 * @returns {undefined}
 */
function deleteMessage(cnv_id, msgid, user_id, thisBox){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: $("#hidden_url").val(),
        cache: 'false',
        data: {cnv_id: cnv_id, msgid: msgid, user_id: user_id},
        timeout: 20000,
        success: function(json){
            if(json.success){
                thisBox.addClass('msg-tbox-deleted');
                $('.msgSwitch li', thisBox).toggle();
            }
        }
    });
}
/**
 * UnDelete a messages 
 * 
 * @param {int/string} cnv_id
 * @param {int/string} msgid
 * @param {int/string} user_id
 * @param {dom/object} thisBox
 * 
 * @returns {undefined}
 */
function undeleteMessage(cnv_id, msgid, user_id, thisBox){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: $("#hidden_url_undelete").val(),
        cache: 'false',
        data: {cnv_id: cnv_id, msgid: msgid, user_id: user_id},
        timeout: 20000,
        success: function(json){
            if(json.success){
                thisBox.removeClass('msg-tbox-deleted');
                $('.msgSwitch li', thisBox).toggle();
                $('.msgSwitch', thisBox).toggle();
            }
        }
    });
}
/* Message Delete END *
/* * * * * * * * * * */

if($(".withChat").length > 0){
    downScroll($(".msg-chat")); // down the scroll of chat
    // emoticon
    $('.msg-tbox p').emoticonize({delay: 1500, animate: false});
}

/**        END of MESSAGES function/action/event :)    **
 **************************************************** **/  



 /*
 * jQuery File Upload Plugin JS Example 8.9.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
 
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
'use strict';
// Change this to the location of your server-side upload handler:
//var url = window.location.hostname === 'blueimp.github.io' ? '//jquery-file-upload.appspot.com/' : '/public/js/blueimpup/server/php/',
//var url = '/public/js/blueimpup/server/php/property.php',
var url = '/catalog/view/javascript/blueimpup/server/php/',
    uploadButton = $('<button/>')
        .addClass('btn btn-primary')
        .prop('disabled', true)
        .text('Caricamento...')
        .on('click', function () {
            var $this = $(this),
                data = $this.data();
            $this
                .off('click')
                .text('Annulla')
                .on('click', function () {
                    $this.remove();
                    data.abort();
                });
            data.submit().always(function () {
                $this.remove();
            });
        });
$('#fileupload').fileupload({
        url: $(this).attr('action'),
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|docx?|ai|eps|tiff?|txt|html)$/i,
        maxFileSize: 6000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        /*disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 150,
        previewMaxHeight: 150,*/
        //sequentialUploads: true,
        limitConcurrentUploads: 1,
        disableImageMetaDataLoad: true,
        disableImageMetaDataSave: true,
        dropZone: '.dropzone'
    }).on('fileuploadadd', function (e, data) {
		data.context = $('<div/>').appendTo('#files');
        /*$.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });*/
    })
	/*.on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Annulla');// .prop('disabled', !!data.files.error);
        }
    })*/
	.on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
		$.each(data.result.files, function (index, file) {
            if (file.url) {
			// type/s = "application\/pdf", "type":"image\/jpeg" ...
				if(file.type.indexOf("image") >= 0){
					sendMessage($('.msg-write form'), file.url, 'file-img', 'File: '+file.title);
				} else {
					sendMessage($('.msg-write form'), file.url, 'file', 'File: '+file.title);
				}
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
				// .children()[index]
                $(data.context).append('<br>').append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('Upload file fallito.');
			//.children()[index]
            $(data.context).append('<br>').append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }); 

    $(document).bind('dragover', function (e) {
        var dropZone = $('.dropzone'),
            timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
            node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        if (found) {
            dropZone.addClass('hover');
        } else {
            dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });

	
	
	
	

// ------------------------------------ //
/** 
 * @general #event listener and setter with all this method 
 */
// ------------------------------------ //

// for loop and pause listen server
var fibonacci = 5, pausen = 2, listenPause;
$(document).mousemove(function(){
    // on mouse move reset pause time
    pausen = 2;
});
/**
 * listen server to new updates
 * @returns null
 */
function listenServer(){
	//var user_id = $("input[name=user_id]",  $(".msg-write")).val();
	
    $.ajax({
        type: 'GET',
        dataType: 'json',
		//data: {user_id: user_id}
        url: $("#hidden_url_listen").val(),
        cache: 'false',
        timeout: 90000,
        success: function(json){
            json = eval(json);
            if(json.success){
                if(json.event == 'new_msg'){
                    for(var key in json.msgs){
						receiveMessages(json.msgs[key].cnv_id, json.msgs[key].msgid, json.msgs[key].msgtxt, json.msgs[key].cnvtxt, json.msgs[key].nmsgs, json.msgs[key].ts, json.msgs[key].user_id, json.msgs[key].msgtype, json.msgs[key].msgtitle);
                    }
                }
            }
            // if pause loop a lot increment the pause time for doing request
            if(pausen > 7)
                fibonacci = (pausen-1) + fibonacci;
            else
                fibonacci = 5;

            // pause loop
            pausen++;
            var pause = fibonacci * 1000;// i need milleseconds
            // if pause are more than 1:30m it are 1:30m
            if(pause > 130000)
                pause = 130000;
            listenPause = setTimeout(function(){listenServer();}, pause);
        },
        error: function(){
            // if pause loop a lot increment the pause time for doing request
            if(pausen > 7)
                fibonacci = (pausen-1) + fibonacci;
            else
                fibonacci = 5;
            // increment pause loop
            pausen++;
            var pause = fibonacci * 1000; // i need milleseconds
            // if pause are more than 1:30m it are 1:30m
            if(pause > 130000)
                pause = 130000;
            setTimeout(function(){listenServer();}, pause);
        }
    });
}




function receiveMessages(cnv_id, msgid, msgtxt, cnvtxt, nmsgs, ts, user_id, msgtype, msgtitle){
	if($("input[name=cnv_id]",  $(".msg-write")).val() == cnv_id && $("#"+msgid, $(".msg-chat")).length == 0){
		var receivedClone = $("#receivedClone", $(".msg-write")).clone(), thisForm = $("form",  $(".msg-write")), user_cid = thisForm.find('input[name=user_id]').val(), cnv_cid = thisForm.find('input[name=cnv_id]').val();
    
		//$(".loader", thisForm).hide();
		//$("input[type=submit]", thisForm).val(json.btext).removeClass('btn-warning').addClass('btn-primary');
		
		//setreadConversation(cnv_id);
		if(msgtype == 'file'){
			var html = '<a href="'+msgtxt+'" target="_blank"><div class="preview docfile"><img class="lfloat" src="../../image/docfile.png"></div>'+msgtitle+'</a>';
			receivedClone.find('p').html(html);
		} else if(msgtype == 'file-img') {
			var html = '<a href="'+msgtxt+'" target="_blank"><div class="preview"><img class="lfloat" src="'+msgtxt+'"></div>'+msgtitle+'</a>';
			receivedClone.find('p').html(html);
		} else {
			receivedClone.find('p').html(msgtxt);
		}
		
		receivedClone.find('.yesDelete').attr('id', msgid);
		receivedClone.find('.cancelDelete').attr('id', msgid);
		receivedClone.attr('title', ts);
		receivedClone.addClass('chat-lnew');
		receivedClone.show().appendTo($(".msg-chat"));
			
		// @maragliata
		receivedClone.animate({backgroundColor: '#BEBEBE',}, 1500 );
		$('p', receivedClone).emoticonize({animate: false, delay: 1500});
		
		receivedClone.removeClass('display-none').attr('id', msgid);
		
		downScroll($(".msg-chat"));
	}
	
	$('#'+cnv_id, $(".msg-users")).removeClass('selected').addClass('new');
}

listenServer(); // listen for news

});