<?php echo $header; ?>
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/css/jquery.fileupload-ui-noscript.css"></noscript>

<?php echo $column_left; ?>
<?php echo $column_right; ?>

<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?>  </h1>
<div class="box">
    <div class="box-content withChat">        
        <div class="newCnv msg-chat-header clearfix">
            <h1 class="lfloat">Ciao <?php echo $this->customer->getFirstname(); ?></h1>
            <?php if(isset($error)): ?>
                <div class="rfloat" style="padding: 10px; border: 1px dashed orange; background-color: red; color: white; font-weight: bold;"><?php echo $error; ?></div>
            <?php endif; ?>
            <?php if(isset($success)): ?>
                <div class="rfloat" style="padding: 10px; border: 1px dashed blue; background-color: green; color: white; font-weight: bold;"><?php echo $success; ?></div>
            <?php endif; ?>
            <div id="msgError" class="display-none" style="padding: 10px; border: 1px dashed orange; background-color: red; color: white; font-weight: bold;"></div>
        </div> 

        <div class="msg-container lfloat">
            <div class="msg-users">
                <div id="<?php echo $this->model_message_message->id; ?>"  class="msg-user <?php if($status == 1): ?>new<?php else: ?>selected<?php endif; ?>">
                    <img src="<?php echo HTTP_SERVER; ?>image/message/admin.png" class="lfloat">
                    <h2><?php echo $admin_name; ?></h2>
                    <p><?php echo $this->model_message_message->rd_last; ?></p>
                </div>
            </div>
			
			<div>
				<?php if(count($last_img) > 1): ?>
				<div title="Messaggio inviato: <?php echo $last_img['ts_created'];?>">
					<h3>Ultimo bozzetto: <small><?php echo $last_img['rdf_created'];?></small></h3>
					<p>
						<a href="<?php echo $last_img['message'];?>" target="_blank">
							<div style="width:200px;">
								<img style="max-width: 100%; display:block; height:auto;" src="<?php echo $last_img['message'];?>">
							</div>
							<?php echo $last_img['title'];?>
						</a>
					</p>
				</div>
				<?php endif; ?>
			</div>
        </div>
        
        <ul class="msg-chat rfloat">            
            <?php if(isset($messages))
                include DIR_TEMPLATE.'default/template/message/libs/messages.tpl';
            ?>
        </ul>

        <div class="msg-write rfloat">
            <form action="<?php echo $action ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="user_id" value="<?php echo $other_user_id; ?>" required>
                <input type="hidden" name="cnv_id" value="<?php echo ($cnv_id > 0) ? $cnv_id : ''; ?>">
				<input type="hidden" id="hidden_url_listen" value="<?php echo $this->url->link('message/message/listen'); ?>">
                <textarea required autofocus class="lfloat" name="msgtxt"></textarea>
                <input type="submit" value="Invia" class="btn btn-success btn-lg">
            </form>

            <div class="clearfix"></div>
            <div class="dropzone rfloat">
                <!-- The file upload form used as target for the file upload widget -->
                <form id="fileupload" action="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/server/php/" method="POST" enctype="multipart/form-data">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                    <noscript>Devi abilitare i Javascript nel Browser per poter caricare immagini (oppure riprova con un altro Borwser es: Chrome, FireFox, Opera)</noscript>
                    
                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                    <div class="row fileupload-buttonbar">
                        <div class="text-right">
                            
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-info fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Invia file...</span>
                                <input type="file" name="files[]">
                            </span>
                            
                            <!-- The global file processing state -->
                            <span class="fileupload-process"></span>
                        </div>
                        
                        <!-- The global progress state -->
                        <div class="fileupload-progress fade">
                            <!-- The global progress bar -->
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                            </div>
                            <!-- The extended global progress state -->
                            <div class="progress-extended">&nbsp;</div>
                        </div>
                    </div>
                    <!-- The table listing the files available for upload/download -->
                    <div role="presentation" class="table files"></div>
                </form>
            </div>
            
            <li id="sentClone" class="msg-tbox msg-tbox-sent display-none">
                <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
                <p></p>
                <span class="date-time">Adesso</span>
            </li>
            <li id="receivedClone" class="msg-tbox msg-tbox-received display-none">
                <img src="<?php echo HTTP_SERVER; ?>image/message/admin.png">
                <p></p>
                <span class="date-time">Adesso</span>
            </li>
        </div>
                
        <div class="clearfix"></div>
    </div>
</div>

  <?php echo $content_bottom; ?>
</div>


<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-process.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/jquery.fileupload-ui.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
    <script src="<?php echo HTTP_SERVER; ?>catalog/view/javascript/blueimpup/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->


<?php echo $footer; ?>