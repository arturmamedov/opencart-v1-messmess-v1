<?php 
    foreach($messages as $msg):
	// this user messages
    if($msg['user_id'] == $this->customer->getId()):
	if($msg['type'] == 'file-img'):
?>
		<!-- img messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
            <p>
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview">
					<img class="lfloat" src="<?php echo $msg['message'];?>">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php elseif($msg['type'] == 'file'): ?>
		<!-- file messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
            <p>
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview docfile">
					<img class="lfloat" src="<?php echo HTTP_SERVER; ?>image/docfile.png">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php else: ?>
		<!-- text messages -->
        <li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-sent" title="Messaggio inviato: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
            <p><?php echo $msg['message'];?></p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
    <?php 
	endif;
    else:
	// other user messages
	if($msg['type'] == 'file-img'):
?>
		<!-- image messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
            <p>
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview">
					<img class="lfloat" src="<?php echo $msg['message'];?>">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php elseif($msg['type'] == 'file'): ?>
		<!-- file messages -->
		<li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/user.png">
            <p>
			<a href="<?php echo $msg['message'];?>" target="_blank">
				<div class="preview docfile">
					<img class="lfloat" src="<?php echo HTTP_SERVER; ?>image/docfile.png">
				</div>
				<?php echo $msg['title'];?>
			</a>
			</p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php else: ?>
		<!-- text messages -->
        <li id="<?php echo $msg['id']; ?>" class="msg-tbox msg-tbox-received" title="Messaggio ricevuto: <?php echo $msg['ts_created'];?>">
            <img src="<?php echo HTTP_SERVER; ?>image/message/admin.png">
            <p><?php echo $msg['message'];?></p>
            <span class="date-time"><?php echo $msg['rdf_created'];?></span>
        </li>
<?php 
	endif;
    endif;
endforeach;
?>