<?php
/**
 * Frontend Message Model
 * @version 1.0  10/03/2014
 * @author arturmamedov1993@gmail.com
 */
class ModelMessageMessage extends Model {
    
    /**
     * Variabile del amministratore che riceverà tutti i messaggi
     * @var integer
     */
	private $other_user_id = 1;
    /**
     * Name of conversations table
     * @var string defined in constructor
     */
    private $_table;
    /**
     * Name of messages table
     * @var string defined in constructor
     */
    private $_messages_table;
    /**
     * Timestamp of time limit for send mail notif
     * @var string defined in constructor
     */
    public $send_mail_ts;
	/**
	 * Fake message array for an empty message or for clone ...
	 * @var array empty message
	 */
	 public $fake_msg = array
		(
			'id' => 0,
			'cnv_id' => 0,
			'user_id' => 0,
			'to_user_id' => 0,
			'title' => '',
			'message' => '',
			'one_active' => 1,
			'two_active' => 1,
			'status' => 0,
			'ts_created' => 0,
			'type' => '',
			'rd_created' => 0,
			'rdf_created' => 0
		);
	
    public function __construct($registry){
        parent::__construct($registry);
        
        // table of conversations // one conversation have the 2 user_id/s 
        $this->_table = DB_PREFIX ."conversations";
        // table of messages // one message are one message of conversation
        $this->_messages_table = DB_PREFIX . "conversations_messages";
        // timestamp
        $this->send_mail_ts = strtotime("-5 minutes");
    }
    
    /**
     * Properieta admin_id
     * 
     * @return integer
     */
    public function getOtherUserId(){
        return $this->other_user_id;
    }
    
    /** 
     * Get conversation for user
     * for frontend only one cnv
     * 
     * @param int $user_id
     * 
     * @return array
     */
    public function getCnv($id){
        $query = "SELECT * FROM {$this->_table} as cnv WHERE id={$id}";
		$result = $this->db->query($query);
        return $result->row;
    }
    
    
    /** 
     * Get conversation for user
     * for frontend only one cnv
     * 
     * @param int $user_id
     * 
     * @return array
     */
    public function getMessages($cnv_id){
        $query = "SELECT * FROM {$this->_messages_table} as cnvm WHERE cnv_id={$cnv_id} AND one_active = 1 AND two_active = 1 ORDER BY id ASC";
		$result = $this->db->query($query);
        
        return $result->rows;
    }
	
	/** 
     * Get only new messages for updates
     *
	 * @param int $user_id to which user id
     * @param int $dt_timestamp timestamp from that compare
     * 
     * @return array
     */
    public function newMessages($user_id, $dt_timestamp){
        $query = "SELECT * FROM {$this->_messages_table} as cnvm WHERE cnv_id = {$this->id} AND to_user_id = {$user_id} AND ts_created > '{$dt_timestamp}' AND one_active = 1 AND two_active = 1 AND status = 1  ORDER BY id ASC";
		$result = $this->db->query($query);
        
		$rows = $result->rows;
		
        if(count($rows) > 0){
			return $rows;
		} else {
			return false;
		}
    }
	
	
	/** 
     * Get the last image from order or messages by admin
     * 
     * @return array message array data
     */
    public function lastimgMessage(){
		$img_message = array(); // the message with image from cnv or order
		
		if($this->user_one_id == 1){
			$user_id = $this->user_two_id;
		} else {
			$user_id = $this->user_one_id;
		}
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.customer_id = '" . (int)$user_id . "' ORDER BY order_id DESC LIMIT 1");

		// get last img message
		$query = "SELECT * FROM {$this->_messages_table} as cnvm WHERE cnv_id = {$this->id} AND type = 'file-img' AND one_active = 1 AND two_active = 1 AND user_id = 1 ORDER BY id DESC LIMIT 1";
		$result = $this->db->query($query);
		$cnv_last_img = $result->row;
		
		// you can use @date_added or @date_modified
		if(isset($order_query->row['date_modified'])){
			$order_date = $order_query->row['date_modified'];
		} else {
			$order_date = 0;
		}
		if(isset($order_query->row['date_added']) && ($order_date == null || $order_date == '0000-00-00 00:00:00')){
			$order_date = $order_query->row['date_added'];
		}

		if($order_query->num_rows != 0 && (count($cnv_last_img) == 0 || $order_date > @$cnv_last_img['ts_created'])){
			$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_query->row['order_id'] . "'");
			
			foreach($order_option_query->rows as $opt){
				if($opt['order_option_id'] == 40 || $opt['type'] == 'file'){
					
					$file = DIR_DOWNLOAD . $opt['value'];
					$mask = basename(utf8_substr($opt['value'], 0, utf8_strrpos($opt['value'], '.')));
					$order_img_name = ($mask ? $mask : basename($file));
					
					$img_message = $this->fake_msg;
					$img_message['title'] = 'Bozzetto: '.$order_img_name;
					$img_message['message'] = HTTP_SERVER . 'download/'.$opt['value'];
					$img_message['type'] = 'img-file';
					
					$img_message['ts_created'] = $order_query->row['date_added'];
					//$date = new DateTime($msg['ts_created']);
					$img_message['rd_created'] = date("d  H:i:s", strtotime($img_message['ts_created'])); //$date->format("d  H:i:s");
					$img_message['rdf_created'] = date("d-m-Y  H:i:s", strtotime($img_message['ts_created'])); //$date->format("d-m-Y  H:i:s");
				}
			}
		} elseif(count($cnv_last_img) > 0) {
			// use $cnv_last_img
			$rows = $this->convertMessages(array($cnv_last_img));
			$img_message = $rows[0]; 
		} else {
			return false;
		}
		
		// return the last image from order or messages by admin
		return $img_message;
    }
	
    
    /** 
     * Get messages count
     * new, all and for 
     * 
     * @param int $user_id
     * 
     * @return array
     */
    public function getMessagesCount($user_id, $what = 'new'){
        switch($what){
            case'new':
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id} AND status = 1";
            break;
            case'all':
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id}";
            break;
            default:
                $query = "SELECT count(*) FROM {$this->_messages_table} as cnvm WHERE to_user_id={$user_id} AND status = 1";
            break;
        }
            
        $result = $this->db->query($query);
        return $result->row['count(*)'];
    }
    
    /** 
     * Convert messages to readable
     * 
     * @param array $rows
     * 
     * @return array Converted
     */
    public function convertMessages($rows){
        foreach($rows as $key => $msg){
			if($rows[$key]['type'] == 'text'){
				// for new line in messages
				$rows[$key]['message'] = nl2br($msg['message']);
				$rows[$key]['message'] = self::convertTextToLink($msg['message']);
			}
			
            //$date = new DateTime($msg['ts_created']);
            $rows[$key]['rd_created'] = date("d  H:i:s", strtotime($msg['ts_created'])); //$date->format("d  H:i:s");
            $rows[$key]['rdf_created'] = date("d-m-Y  H:i:s", strtotime($msg['ts_created'])); //$date->format("d-m-Y  H:i:s");
        }
        return $rows;
    }
	
	 /**
	 * Static function for convert text/html by passing options array
	 * 
	 * @param string $text
	 * @param array $options (if target-blank to false): link havent the target=_blank
	 *  
	 * @return string The converte string
	 */
    static public function convertTextToLink($text, $options = array()){
        //* initialize the default filters
        $defaults = array('target-blank' => true);
        foreach($defaults as $k => $v){
            $options[$k] = array_key_exists($k, $options) ? $options[$k] : $v;
        }
        
        /* Url link */
        if($options['target-blank']){
            // Find and replace all http/https links that are not part of an existing html anchor
            // Find and replace all naked www.links.com (without http://)
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3" target="_blank">$1$3</a>', $text);//@todo pass on a mind page for analytics and more security
        } else 
            $text = preg_replace('/\b(?:(http(s?):\/\/)|(?=www\.))(\S+)/is', '<a href="http$2://$3">$1$3</a>', $text); // @todo pass on a mind page for analytics and more security
        
        return $text;
    }
    
    /**
     * If passed $row with conversation data array 
     * load this data into object, id passed cnv_id SELEcT cnv from db and set to object
     * 
     * @param array/integer $row
     * 
     * @return boolean true
     */
    public function load($row){
        if(is_array($row)){
            $this->id = $row['id'];
            $this->user_one_id = $row['user_one_id'];
            $this->user_two_id = $row['user_two_id'];
            // last_active -
            $this->one_last_active = $row['one_last_active']; // "ultima volta online"
            $this->two_last_active = $row['two_last_active']; // piu che altro ultima azione
            // status - 1 dalegger, 0 letto
            $this->one_status = $row['one_status'];
            $this->two_status = $row['two_status'];
            // path - INBOX, TRASH
            $this->one_path = $row['one_path'];
            $this->two_path = $row['two_path'];
            $this->ts_last = $row['ts_last'];
            
            $date = new DateTime($this->ts_last);
            $this->rd_last = $date->format("d-m-Y  H:i:s");
        } elseif($row > 0) {
            $query = sprintf("SELECT * FROM %s WHERE id = {$row}", $this->_table);

            $result = $this->db->query($query);
            $row = $result->row;

            $this->load($row);
        }
        return true;
    }
    
    /**
     * Load conversation from the two user_ids
     * or create new
     * 
     * @param int $user_one_id
     * @param int $user_two_id
     * 
     * @return boolean
     * 
     * @throws Exception
     */
    public function loadByUsers($user_one_id, $user_two_id){
        $query = sprintf("SELECT * FROM %s WHERE (user_one_id = '{$user_one_id}' AND user_two_id = '{$user_two_id}') OR (user_one_id = '{$user_two_id}' AND user_two_id = '{$user_one_id}')", $this->_table);

        $result = $this->db->query($query);
        // ricevere in array associativo
        if(!$row = $result->row){
            $row = $this->createConversation($user_one_id, $user_two_id, 0);
        }
        
        $this->load($row);
        
        return $row;
    }
     
    /**
     * Create conversation on first send
     * 
     * @param int $user_one_id
     * @param int $user_two_id
     * 
     * @return integer
     * 
     * @throws Exception
     */
    public function createConversation($user_one_id, $user_two_id, $status = 1){
        $this->db->query("INSERT INTO `{$this->_table}` (
                `id`,
                `user_one_id` ,
                `user_two_id` ,
                `two_status`
        ) VALUES (
            NULL , '{$user_one_id}', '{$user_two_id}', '{$status}'
        )");
        
        return $this->db->getLastId();
    }
  
    /**
     * Create message on send
     * 
     * @param int $user_id // from user id
     * @param string $message
     * @param string $type text/file/file-img
     * @param string $title title if file
	 *
     * @return boolean
     * 
     * @throws Exception
     */
    public function createMessage($user_id, $message, $type, $title = null){
        $message = addslashes($message);
        
        if($this->user_one_id == $user_id){
            $other_user_id = $this->user_two_id;
            $this->two_status = 1;
            $this->one_last_active = date('Y-m-d H:i:s');
            $other_online = (strtotime($this->two_last_active) < $this->send_mail_ts) ? false : true;
        } elseif($this->user_two_id == $user_id){
            $other_user_id = $this->user_one_id;
            $this->one_status = 1;
            $this->two_last_active = date('Y-m-d H:i:s');
            $other_online = (strtotime($this->one_last_active) < $this->send_mail_ts) ? false : true;
        } else {
            return 0; // false
        }
        
        // quando creiamo il messaggio mettiamo la conversazione in INBOX
        $this->one_path = 'INBOX';
        $this->two_path = 'INBOX';
        
		$this->ts_last = date('Y-m-d H:i:s');
        $this->save();
        
        $this->db->query("INSERT INTO `{$this->_messages_table}` (
                `id`,
                `cnv_id`,
                `user_id` ,
                `to_user_id`,
				`title`,
                `message`,
                `one_active`,
                `two_active`,
                `status`,
				`type`
        ) VALUES (
            NULL , {$this->id}, {$user_id}, {$other_user_id}, '{$title}', '{$message}', 1, 1, 1, '{$type}'
        )");
        $msgid = $this->db->getLastId();
        
        if(!$other_online){
            $this->sendMail($message, $user_id);
        }
        
        return $msgid;
    }
    
    /**
     * Send notif mail to user (new message received on site)
     * [called in $this->createMessage() if other user offline]
     * 
     * @param string $msgtxt text of new message received
     * @param int $user_id id of user that send message (perform action)
     * 
     * @return boolean true if send are ok
     */
    public function sendMail($msgtxt, $user_id){
		$this->language->load('message/message');

		$this->load->model('account/customer');
		$customer = $this->model_account_customer->getCustomer($user_id);
		$other_user = $this->model_account_customer->getCustomer($this->getOtherUserId());
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($other_user['email']);
		$mail->setFrom($customer['email']);
		$mail->setSender($customer['firstname'].' '.$customer['lastname']);
			
		$mail->setSubject($this->language->get('mail_subject'));
		$mail->setText(strip_tags(html_entity_decode(sprintf($this->language->get('mail_text'), $other_user['firstname'].' '.$other_user['lastname'], $msgtxt), ENT_QUOTES, 'UTF-8')));
		//$mail->setHtml(sprintf($this->language->get('mail_text'), $customer['firstname'].' '.$customer['lastname'], $customer['invite_code']));
		$mail->send();

		return true;
	}
    
     /**
     * Save data of conversation
     * get all param from this object
     * 
     * @return boolean
     */
    public function save(){        
        $query = sprintf("
            UPDATE {$this->_table} SET  
                    `one_last_active` = '%s', 
                    `two_last_active` = '%s',
                    `one_status` = %d, 
                    `two_status` = %d,
                    `one_path` = '%s',
                    `two_path` = '%s',
                    `ts_last` = '%s'
            WHERE id = {$this->id}",
        $this->one_last_active, $this->two_last_active, $this->one_status, $this->two_status, $this->one_path, $this->two_path, $this->ts_last);
        
        $this->db->query($query);
        
        return true;
    }
    
    
    /**
     * Set to 0 a conversation user status
     * 
     * @param int $user_id // the user that call id
     * 
     * @return boolean true/false if not setted
     */
    public function setRead($user_id){
        if($this->user_one_id == $user_id){
            $this->one_status = 0; // one , read
			$this->one_last_active = date('Y-m-d H:i:s');
        } elseif($this->user_two_id == $user_id) {
            $this->two_status = 0; // two, read
			$this->two_last_active = date('Y-m-d H:i:s');
        }
        
        // salva i nuovi dati
        $this->save();
        
        $this->setMessageRead($this->id, $user_id);
        
        return true;
    }
    
    
    /**
     * Set to 0 a messages user status
     * 
     * @param int $cnv_id
     * @param int $user_id
     * 
     * @return boolean true
     */
    public function setMessageRead($cnv_id, $user_id){
        $query = sprintf("UPDATE %s SET  `status` = '0' WHERE `status` = 1 AND cnv_id = %d AND to_user_id = %d", $this->_messages_table, $cnv_id, $user_id);
        // ORDER BY id DESC LIMIT 1; // set read all now

        $this->db->query($query);
        return $this->db->getLastId();
    }
}
?>